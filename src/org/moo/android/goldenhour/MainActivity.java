package org.moo.android.goldenhour;

import java.util.GregorianCalendar;

import org.moo.android.goldenhour.weather.WeatherManager;
import org.moo.widgets.InfiniteViewPager;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends Activity implements LocationListener
{
	private static final String TAG = "GoldenHour";
	private static final float SIGNIFIGANT_UPDATE = 0.75f;
	
	private LocationManager _locationManager;
	private WeatherManager _weatherManager;
	
	private InfiniteViewPager _pager;
	private DailyPagerAdapter _adapter;
	
	private Location _previousLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		_locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		_locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		_locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		
		_weatherManager = WeatherManager.getInstance();
		
		_pager = (InfiniteViewPager)findViewById(R.id.pager);
		_adapter = new DailyPagerAdapter(this, new GregorianCalendar());
		_pager.setAdapter(_adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onLocationChanged(Location location)
	{
		if(_previousLocation == null || moreAccurate(_previousLocation, location))
		{
			float accuracyDelta = _previousLocation == null ? location.getAccuracy() : _previousLocation.getAccuracy() - location.getAccuracy();
			Log.d(TAG, "Updating Location: " + String.format(getResources().getString(R.string.distance_meters), accuracyDelta) + " more accurate");
			Log.d(TAG, "New accuracy: " + String.format(getResources().getString(R.string.distance_meters), location.getAccuracy()));
			_weatherManager.setLocation(location);
			_adapter.setLocation(location);
			_previousLocation = location;
		}
	}

	@Override
	public void onProviderDisabled(String provider)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras)
	{
		// TODO Auto-generated method stub
		
	}
	
	private boolean moreAccurate(Location old, Location update)
	{
		if(!old.hasAccuracy()) return true;
		else if(!update.hasAccuracy()) return false;
		else return update.getAccuracy() < old.getAccuracy() * SIGNIFIGANT_UPDATE;
	}
}
