package productions.moo.goldenhour;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import productions.moo.goldenhour.calc.SolarCalculations;
import productions.moo.goldenhour.calc.SolarCalculations.SolarSeries;
import productions.moo.goldenhour.data.WeatherContract.WeatherEntry;
import productions.moo.goldenhour.weather.WeatherManager;

public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, OnMapReadyCallback
{
	private static final String TAG = DetailFragment.class.getSimpleName();
	private static final int DETAIL_LOADER = 0;

	public static final String LAT_KEY = "lat";
	public static final String LON_KEY = "lon";
	public static final String DATE_KEY = "date";

	private final SimpleDateFormat _dateFormat;
	private final SimpleDateFormat _timeFormat;

	private double _lat, _lon;
	private Date _date;
	private TextView _blueStartTime, _blueEndTime;
	private TextView _goldStartTime, _goldEndTime;
	private TextView _sunriseTime, _sunsetTime;
	private TextView _tempMorning, _tempEvening;
	private TextView _precipMorning, _precipEvening;
	private TextView _windSpeedMorning, _windSpeedEvening;
	private ImageView _weatherIconMorning, _weatherIconEvening;
	private TextView _dateLabel;

    private GoogleMap _map;

	public static DetailFragment newInstance(double lat, double lon, Date date)
	{
		DetailFragment fragment = new DetailFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(DATE_KEY, date);
		bundle.putDouble(LAT_KEY, lat);
		bundle.putDouble(LON_KEY, lon);
		fragment.setArguments(bundle);
		return fragment;
	}

	public DetailFragment()
	{
		setRetainInstance(true);
		_dateFormat = new SimpleDateFormat("MMM dd, yyyy");
		_timeFormat = new SimpleDateFormat("hh:mm a");
	}

    public void updateData(LatLng latLng, Date date)
    {
        _date = date;
        setLocation(latLng);
    }

    public void setLocation(LatLng latLon)
    {
        _lat = latLon.latitude;
        _lon = latLon.longitude;
        getLoaderManager().restartLoader(DETAIL_LOADER, null, this);

        _map.clear();
        _map.addMarker(new MarkerOptions()
                .title("Current Location")
                .position(latLon));
    }

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		Bundle bundle = getArguments();
		_lat = bundle.getDouble(LAT_KEY);
		_lon = bundle.getDouble(LON_KEY);
		_date = (Date)bundle.getSerializable(DATE_KEY);

		getLoaderManager().initLoader(DETAIL_LOADER, null, this);
	}

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();

        FragmentManager manager = ((FragmentActivity)activity).getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.map_container, mapFragment).commit();

        mapFragment.getMapAsync(this);
    }

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

		_blueStartTime = (TextView)rootView.findViewById(R.id.blue_start);
		_blueEndTime = (TextView)rootView.findViewById(R.id.blue_end);
		_goldStartTime = (TextView)rootView.findViewById(R.id.golden_start);
		_goldEndTime = (TextView)rootView.findViewById(R.id.golden_end);
		_sunriseTime = (TextView)rootView.findViewById(R.id.sunrise);
		_sunsetTime = (TextView)rootView.findViewById(R.id.sunset);
		_tempMorning = (TextView)rootView.findViewById(R.id.sunrise_temp);
		_tempEvening = (TextView)rootView.findViewById(R.id.sunset_temp);
		_precipMorning = (TextView)rootView.findViewById(R.id.sunrise_precip);
		_precipEvening = (TextView)rootView.findViewById(R.id.sunset_precip);
		_windSpeedMorning = (TextView)rootView.findViewById(R.id.sunrise_wind);
		_windSpeedEvening = (TextView)rootView.findViewById(R.id.sunset_wind);
		_weatherIconMorning = (ImageView)rootView.findViewById(R.id.sunrise_weather_icon);
		_weatherIconEvening = (ImageView)rootView.findViewById(R.id.sunset_weather_icon);
		_dateLabel = (TextView)rootView.findViewById(R.id.date);

		return rootView;
	}

	@Override
	public void onResume()
	{
		super.onResume();
		getLoaderManager().restartLoader(DETAIL_LOADER, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args)
	{
		Uri uri = WeatherEntry.buildWeatherLocationWithDate(_lat, _lon, _date);
		return  new CursorLoader(
				getActivity(),
				uri,
				null,
				null,
				null,
				WeatherEntry.COLUMN_HOURTEXT + " ASC");
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data)
	{
		_dateLabel.setText(_dateFormat.format(_date));

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(_date);
		SolarSeries series = SolarCalculations.calculate(calendar, _lat, _lon);

		_blueStartTime.setText(_timeFormat.format(series.BlueHourStart.time));
		_blueEndTime.setText(_timeFormat.format(series.BlueHourEnd.time));

		_goldStartTime.setText(_timeFormat.format(series.GoldenHourStart.time));
		_goldEndTime.setText(_timeFormat.format(series.GoldenHourEnd.time));

		_sunriseTime.setText(_timeFormat.format(series.Sunrise.time));
		_sunsetTime.setText(_timeFormat.format(series.Sunset.time));

		String defaultValue = getActivity().getString(R.string.default_value);
		_tempMorning.setText(defaultValue);
		_precipMorning.setText(defaultValue);
		_windSpeedMorning.setText(defaultValue);
		_weatherIconMorning.setImageResource(R.drawable.weather_na);

		_tempEvening.setText(defaultValue);
		_precipEvening.setText(defaultValue);
		_windSpeedEvening.setText(defaultValue);
		_weatherIconEvening.setImageResource(R.drawable.weather_na);

		if(data.moveToFirst())
		{
			do
			{
				int hour = Integer.parseInt(data.getString(data.getColumnIndex(WeatherEntry.COLUMN_HOURTEXT)));
				if(hour == series.Sunrise.time.getHours())
				{
					_tempMorning.setText(getActivity().getString(R.string.format_temperature,
							data.getFloat(data.getColumnIndex(WeatherEntry.COLUMN_TEMPERATURE))));
					_precipMorning.setText(getActivity().getString(R.string.format_precip,
							data.getFloat(data.getColumnIndex(WeatherEntry.COLUMN_PRECIPITATION)) * 100.0f));
					_windSpeedMorning.setText(getActivity().getString(R.string.format_mph,
							data.getFloat(data.getColumnIndex(WeatherEntry.COLUMN_WIND_SPEED))));
					_weatherIconMorning.setImageResource(WeatherManager.StringToResource(
							data.getString(data.getColumnIndex(WeatherEntry.COLUMN_WEATHER_ICON))));
				}
				else if(hour == series.Sunset.time.getHours())
				{
					_tempEvening.setText(getActivity().getString(R.string.format_temperature,
							data.getFloat(data.getColumnIndex(WeatherEntry.COLUMN_TEMPERATURE))));
					_precipEvening.setText(getActivity().getString(R.string.format_precip,
							data.getFloat(data.getColumnIndex(WeatherEntry.COLUMN_PRECIPITATION)) * 100.0f));
					_windSpeedEvening.setText(getActivity().getString(R.string.format_mph,
							data.getFloat(data.getColumnIndex(WeatherEntry.COLUMN_WIND_SPEED))));
					_weatherIconEvening.setImageResource(WeatherManager.StringToResource(
							data.getString(data.getColumnIndex(WeatherEntry.COLUMN_WEATHER_ICON))));
				}
			}while(data.moveToNext());
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader)
	{

	}

	@Override
	public void onMapReady(GoogleMap map)
	{
		LatLng location = new LatLng(_lat, _lon);

        _map = map;

		map.setMyLocationEnabled(true);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 13));

		map.addMarker(new MarkerOptions()
				.title("Current Location")
				.position(location));
	}
}