package productions.moo.goldenhour;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

import productions.moo.goldenhour.data.WeatherContract;
import productions.moo.goldenhour.data.WeatherContract.LocationEntry;
import productions.moo.goldenhour.data.WeatherContract.WeatherEntry;
import productions.moo.goldenhour.weather.WeatherAdapter;
import productions.moo.goldenhour.weather.WeatherService;

public class DateListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>
{
    private static final int FORECAST_LOADER = 0;
    private static final String[] FORECAST_COLUMNS = {
            WeatherEntry.TABLE_NAME + "." + WeatherEntry._ID,
            WeatherEntry.COLUMN_DATETEXT,
            WeatherEntry.COLUMN_HOURTEXT,
            WeatherEntry.COLUMN_TEMPERATURE,
            WeatherEntry.COLUMN_WEATHER_ICON,
            LocationEntry.COLUMN_COORD_LAT,
            LocationEntry.COLUMN_COORD_LONG
    };

    public static final int COL_WEATHER_ID = 0;
    public static final int COL_WEATHER_DATE = 1;
    public static final int COL_WEATHER_HOUR = 2;
    public static final int COL_WEATHER_TEMP = 3;
    public static final int COL_WEATHER_ICON = 4;
    public static final int COL_LOCATION_LAT = 5;
    public static final int COL_LOCATION_LON = 6;
    private static final String TAG = "WeatherListFragment";

    private ListView _list;
    private OnDateSelectionListener _listener;
    private WeatherAdapter _adapter;

    private double _lat, _lon;
    public static DateListFragment newInstance()
    {
        DateListFragment fragment = new DateListFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DateListFragment()
    {
	    setRetainInstance(true);
    }

    public void setLocation(LatLng latLon)
    {
        _lat = latLon.latitude;
	    _lon = latLon.longitude;
        WeatherService.startWeatherQuery(getActivity(), _lat, _lon);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_datelist, container, false);
        _list = (ListView)view.findViewById(R.id.list);
        _adapter = new WeatherAdapter(getActivity());
        _list.setAdapter(_adapter);
        _list.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
	            Cursor cursor = _adapter.getCursor();
	            if(cursor != null && cursor.moveToPosition(position))
	            {
		            String dateText = cursor.getString(cursor.getColumnIndex(WeatherEntry
				            .COLUMN_DATETEXT));
		            _listener.onSelection(WeatherContract.getDateFromDatabase(dateText), _lat, _lon);
	            }
            }
        });
        return view;
    }

	@Override
	public void onResume()
	{
		super.onResume();
		getLoaderManager().restartLoader(FORECAST_LOADER, null, this);
	}

	@Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            _listener = (OnDateSelectionListener)activity;

        }
        catch(ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        getLoaderManager().initLoader(FORECAST_LOADER, null, this);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        _listener = null;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle)
    {
        String sortOrder = WeatherContract.WeatherEntry.COLUMN_DATETEXT + " ASC";
        Uri weatherForLocationUri = WeatherContract.WeatherEntry.buildWeatherLocationWithStartDate(_lat, _lon, new Date());

        return new CursorLoader(getActivity(), weatherForLocationUri, null, null, null, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor)
    {
        Log.d(TAG, "Cursor Count: " + cursor.getCount());
        _adapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader)
    {
        _adapter.swapCursor(null);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnDateSelectionListener
    {
        public void onSelection(Date date, double lat, double lon);
    }
}
