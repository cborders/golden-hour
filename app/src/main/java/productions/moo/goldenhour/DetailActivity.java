package productions.moo.goldenhour;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import java.util.Date;

public class DetailActivity extends ActionBarActivity
{
	private DetailFragment _fragment;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		if(savedInstanceState == null)
		{
			Date date = (Date)getIntent().getSerializableExtra(DetailFragment.DATE_KEY);
			double lat = getIntent().getDoubleExtra(DetailFragment.LAT_KEY, 0);
			double lon = getIntent().getDoubleExtra(DetailFragment.LON_KEY, 0);

			_fragment = DetailFragment.newInstance(lat, lon, date);

			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, _fragment)
					.commit();
		}
	}
}
