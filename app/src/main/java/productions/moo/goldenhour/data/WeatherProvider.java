package productions.moo.goldenhour.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import productions.moo.goldenhour.data.WeatherContract.LocationEntry;
import productions.moo.goldenhour.data.WeatherContract.WeatherEntry;

/**
 * Created by cborders on 12/4/14.
 */
public class WeatherProvider extends ContentProvider
{
    private static final int WEATHER = 100;
    private static final int WEATHER_WITH_LOCATION = 101;
    private static final int WEATHER_WITH_LOCATION_AND_DATE = 102;
    private static final int WEATHER_WITH_LOCATION_DATE_AND_HOUR = 103;
    private static final int LOCATION = 300;
    private static final int LOCATION_ID = 301;

    private static final UriMatcher _uriMatcher = buildUriMatcher();

    private DatabaseHelper _databaseHelper;
    private static final SQLiteQueryBuilder _weatherByLocationQueryBuilder;

    static
    {
        _weatherByLocationQueryBuilder = new SQLiteQueryBuilder();
        _weatherByLocationQueryBuilder.setTables(
                WeatherEntry.TABLE_NAME + " INNER JOIN " + LocationEntry.TABLE_NAME + " ON " +
                WeatherEntry.TABLE_NAME + "." + WeatherEntry.COLUMN_LOC_KEY + " = " +
                LocationEntry.TABLE_NAME + "." + LocationEntry._ID
        );
    }

    private static UriMatcher buildUriMatcher()
    {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = WeatherContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, WeatherContract.PATH_WEATHER, WEATHER);
        matcher.addURI(authority, WeatherContract.PATH_WEATHER + "/*", WEATHER_WITH_LOCATION);
        matcher.addURI(authority, WeatherContract.PATH_WEATHER + "/*/*", WEATHER_WITH_LOCATION_AND_DATE);
        matcher.addURI(authority, WeatherContract.PATH_WEATHER + "/*/*/#", WEATHER_WITH_LOCATION_DATE_AND_HOUR);

        matcher.addURI(authority, WeatherContract.PATH_LOCATION, LOCATION);
        matcher.addURI(authority, WeatherContract.PATH_LOCATION + "/#", LOCATION_ID);

        return matcher;
    }

    public static final String LocationSelection =
            LocationEntry.TABLE_NAME  + "." + LocationEntry.COLUMN_COORD_LAT + " = ? AND " +
            LocationEntry.TABLE_NAME  + "." + LocationEntry.COLUMN_COORD_LONG + " = ?";

    private static final String LocationWithStartDateSelection =
            LocationSelection + " AND " + WeatherEntry.COLUMN_DATETEXT + " >= ?";

    private static final String LocationWithDateSelection =
            LocationSelection + " AND " + WeatherEntry.COLUMN_DATETEXT + " = ?";

    private static final String LocationWithDateAndHourSelection =
            LocationSelection + " AND " + WeatherEntry.COLUMN_DATETEXT + " = ? AND " +
                    WeatherEntry.COLUMN_HOURTEXT + " = ?";

    private Cursor getWeatherByLocation(Uri uri, String[] projection, String sortOrder)
    {
        String[] parts = WeatherEntry.getLocationFromUri(uri);
        String startDate = WeatherEntry.getStartDateFromUri(uri);

        String[] selectionArgs;
        String selection;

        if(startDate == null)
        {
            selection = LocationSelection;
            selectionArgs = new String[]{parts[0], parts[1]};
        }
        else
        {
            selection = LocationWithStartDateSelection;
            selectionArgs = new String[]{parts[0], parts[1],startDate};
        }

        return _weatherByLocationQueryBuilder.query(_databaseHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                WeatherEntry.COLUMN_DATETEXT,
                null,
                sortOrder);
    }

    private Cursor getWeatherByLocationWithDate(Uri uri, String[] projection, String sortOrder)
    {
        String[] parts = WeatherEntry.getLocationFromUri(uri);
        String day = WeatherEntry.getDateFromUri(uri);
        String hour = WeatherEntry.getHourFromUri(uri);

        String selection;
        String[] selectionArgs;

        if(hour == null)
        {
            selection = LocationWithDateSelection;
            selectionArgs = new String[]{parts[0], parts[1], day};
        }
        else
        {
            selection = LocationWithDateAndHourSelection;
            selectionArgs = new String[]{parts[0], parts[1], day, hour};
        }

        return _weatherByLocationQueryBuilder.query(_databaseHelper.getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);
    }

    @Override
    public boolean onCreate()
    {
        _databaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri)
    {
        final int match = _uriMatcher.match(uri);
        switch(match)
        {
            case WEATHER_WITH_LOCATION_DATE_AND_HOUR:
                return WeatherEntry.CONTENT_ITEM_TYPE;
            case WEATHER_WITH_LOCATION_AND_DATE:
                return WeatherEntry.CONTENT_TYPE;
            case WEATHER_WITH_LOCATION:
                return WeatherEntry.CONTENT_TYPE;
            case WEATHER:
                return WeatherEntry.CONTENT_TYPE;
            case LOCATION_ID:
                return LocationEntry.CONTENT_ITEM_TYPE;
            case LOCATION:
                return LocationEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        Cursor retCursor;
        switch(_uriMatcher.match(uri))
        {
            case WEATHER_WITH_LOCATION_DATE_AND_HOUR:
            {
                retCursor = getWeatherByLocationWithDate(uri, projection, sortOrder);
            } break;
            case WEATHER_WITH_LOCATION_AND_DATE:
            {
                retCursor = getWeatherByLocationWithDate(uri, projection, sortOrder);
            } break;
            case WEATHER_WITH_LOCATION:
            {
                retCursor = getWeatherByLocation(uri, projection, sortOrder);
            } break;
            case WEATHER:
            {
                retCursor = _databaseHelper.getReadableDatabase().query(
                        WeatherEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
            } break;
            case LOCATION_ID:
            {
                retCursor = _databaseHelper.getReadableDatabase().query(
                        LocationEntry.TABLE_NAME,
                        projection,
                        LocationEntry._ID + " = '" + ContentUris.parseId(uri) + "'",
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
            } break;
            case LOCATION:
            {
                retCursor = _databaseHelper.getReadableDatabase().query(
                        LocationEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
            } break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        retCursor.setNotificationUri(getContext().getContentResolver(), uri);

        return retCursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        SQLiteDatabase database = _databaseHelper.getWritableDatabase();
        Uri returnUri;

        switch(_uriMatcher.match(uri))
        {
            case WEATHER:
            {
                long id = database.insert(WeatherEntry.TABLE_NAME, null, values);
                if(id > 0) returnUri = WeatherEntry.buildWeatherUri(id);
                else throw new SQLException("Failed to insert row into " + uri);
            } break;
            case LOCATION:
            {
                long id = database.insert(LocationEntry.TABLE_NAME, null, values);
                if(id > 0) returnUri = LocationEntry.buildLocationUri(id);
                else throw new SQLException("Failed to insert row into " + uri);
            } break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        SQLiteDatabase database = _databaseHelper.getWritableDatabase();
        int rowsAffected;

        switch(_uriMatcher.match(uri))
        {
            case WEATHER:
            {
                rowsAffected = database.delete(WeatherEntry.TABLE_NAME, selection, selectionArgs);
            } break;
            case LOCATION:
            {
                rowsAffected = database.delete(LocationEntry.TABLE_NAME, selection, selectionArgs);
            } break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsAffected;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
    {
        SQLiteDatabase database = _databaseHelper.getWritableDatabase();
        int rowsAffected;

        switch(_uriMatcher.match(uri))
        {
            case WEATHER:
            {
                rowsAffected = database.update(WeatherEntry.TABLE_NAME, values, selection, selectionArgs);
            } break;
            case LOCATION:
            {
                rowsAffected = database.update(LocationEntry.TABLE_NAME, values, selection, selectionArgs);
            } break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsAffected;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values)
    {
        final SQLiteDatabase database = _databaseHelper.getWritableDatabase();
        final int match = _uriMatcher.match(uri);
        switch(match)
        {
            case WEATHER:
            {
                database.beginTransaction();
                int returnCount = 0;
                try
                {
                    for(ContentValues value : values)
                    {
                        long id = database.insert(WeatherEntry.TABLE_NAME, null, value);
                        if(- 1 != id) returnCount++;
                    }
                    database.setTransactionSuccessful();
                }
                finally
                {
                    database.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            }
            default:
            {
                return super.bulkInsert(uri, values);
            }
        }
    }
}
