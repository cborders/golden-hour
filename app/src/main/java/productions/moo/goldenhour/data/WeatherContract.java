/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package productions.moo.goldenhour.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Defines table and column names for the weather database.
 */
public class WeatherContract
{
    public static final String CONTENT_AUTHORITY = "productions.moo.goldenhour.app";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_WEATHER = "weather";
    public static final String PATH_LOCATION = "location";

    /* Inner class that defines the table contents of the weather table */
    public static final class WeatherEntry implements BaseColumns
    {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_WEATHER).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_WEATHER;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_WEATHER;

        public static final String TABLE_NAME = "weather";

        // Column with the foreign key into the location table.
        public static final String COLUMN_LOC_KEY = "location_id";

        public static final String COLUMN_DATETEXT = "date";
        public static final String COLUMN_HOURTEXT = "hour";
        public static final String COLUMN_WEATHER_ICON = "weather_icon";
        public static final String COLUMN_SHORT_DESC = "short_desc";
        public static final String COLUMN_TEMPERATURE = "temp";

        // Humidity is stored as a float representing percentage
        public static final String COLUMN_PRECIPITATION = "precipitation";

        // Wind speed is stored as a float representing wind speed Km/h
        public static final String COLUMN_WIND_SPEED = "wind";

        // Degrees are meteorological degrees (e.g, 0 is north, 180 is south).  Stored as floats.
        public static final String COLUMN_WIND_DEGREES = "wind_degrees";

        public static Uri buildWeatherUri(long id)
        {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildWeatherLocation(double lat, double lon)
        {
            return CONTENT_URI.buildUpon().appendPath(getLocationString(lat, lon)).build();
        }

        public static final Uri buildWeatherLocationWithStartDate(double lat, double lon, Date date)
        {
            return CONTENT_URI.buildUpon()
                    .appendPath(getLocationString(lat, lon))
                    .appendQueryParameter(COLUMN_DATETEXT, getDatabaseDateString(date))
                    .build();
        }

        public static Uri buildWeatherLocationWithDate(double lat, double lon, Date date)
        {
            return CONTENT_URI.buildUpon()
                    .appendPath(getLocationString(lat, lon))
                    .appendPath(getDatabaseDateString(date))
                    .build();
        }

        public static Uri buildWeatherLocationWithDateAndHour(double lat, double lon, Date date)
        {
            return CONTENT_URI.buildUpon()
                    .appendPath(getLocationString(lat, lon))
                    .appendPath(getDatabaseDateString(date))
                    .appendPath(getDatabaseHourString(date))
                    .build();
        }

        public static String[] getLocationFromUri(Uri uri)
        {
            return uri.getPathSegments().get(1).split(",");
        }

        public static String getDateFromUri(Uri uri)
        {
            return uri.getPathSegments().get(2);
        }

        public static String getHourFromUri(Uri uri)
        {
	        List<String> segments = uri.getPathSegments();
	        if(segments.size() <= 3) return null;
            else return segments.get(3);
        }

        public static String getStartDateFromUri(Uri uri)
        {
            return uri.getQueryParameter(COLUMN_DATETEXT);
        }

        public static String getLocationString(double lat, double lon)
        {
            return String.valueOf(LocationEntry.roundToLessPrecision(lat)) + "," + String.valueOf(LocationEntry.roundToLessPrecision(lon));
        }
    }

    public static final class LocationEntry implements BaseColumns
    {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_LOCATION).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;

        public static final String TABLE_NAME = "location";
        public static final String COLUMN_COORD_LAT = "coord_lat";
        public static final String COLUMN_COORD_LONG = "coord_long";
        public static final String COLUMN_LOCATION_STRING = "location_string";

        public static Uri buildLocationUri(long id)
        {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static double roundToLessPrecision(double value)
        {
            return Math.round(value * 1000.0) / 1000.0;
        }
    }

    public static final String DATE_FORMAT = "yyyyMMdd";
    public static final String DATETIME_FORMAT = "yyyyMMdd hh:mm";
    public static final String HOUR_FORMAT = "HH";

    public static String getDatabaseDateString(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        return format.format(date);
    }

    public static String getDatabaseHourString(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat(HOUR_FORMAT);
        return format.format(date);
    }

    /**
     * Converts a dateText to a long Unix time representation
     *
     * @param dateText the input date string
     * @return the Date object
     */
    public static Date getDateFromDatabase(String dateText)
    {
        SimpleDateFormat databaseDateFormat = new SimpleDateFormat(DATE_FORMAT);
        try
        {
            return databaseDateFormat.parse(dateText);
        }
        catch(ParseException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Converts a dateText to a long Unix time representation
     *
     * @param dateText the input date string
     * @return the Date object
     */
    public static Date getDateTimeFromDatabase(String dateText)
    {
        SimpleDateFormat databaseDateFormat = new SimpleDateFormat(DATETIME_FORMAT);
        try
        {
            return databaseDateFormat.parse(dateText);
        }
        catch(ParseException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}