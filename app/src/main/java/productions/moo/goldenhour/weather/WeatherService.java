package productions.moo.goldenhour.weather;

import android.app.IntentService;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import productions.moo.goldenhour.data.WeatherContract;
import productions.moo.goldenhour.data.WeatherContract.WeatherEntry;
import productions.moo.goldenhour.data.WeatherContract.LocationEntry;
import productions.moo.goldenhour.data.WeatherProvider;

public class WeatherService extends IntentService
{
    private static final String EXTRA_LATITUDE = "productions.moo.goldenhour.weather.extra.LATITUDE";
    private static final String EXTRA_LONGITUDE = "productions.moo.goldenhour.weather.extra.LONGITUDE";
    private static final String EXTRA_DATE = "productions.moo.goldenhour.weather.extra.DATE";

    private static final String TAG = "WeatherService";
    private static final String API_KEY = "013a4631b907187fffb217784ef459e6";
    private static final String BASE_URI = "https://api.forecast.io/forecast/" + API_KEY + "/";

    public WeatherService()
    {
        super("WeatherService");
    }

    public static void startWeatherQuery(Context context, double lat, double lon)
    {
        Intent intent = new Intent(context, WeatherService.class);
        intent.putExtra(EXTRA_LATITUDE, lat);
        intent.putExtra(EXTRA_LONGITUDE, lon);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
	    if(intent != null)
	    {
		    double lat = LocationEntry.roundToLessPrecision(intent.getDoubleExtra(EXTRA_LATITUDE, 0));


		    double lon = LocationEntry.roundToLessPrecision(intent.getDoubleExtra(EXTRA_LONGITUDE,
				    0));

		    Cursor cursor = getContentResolver().query(
				    LocationEntry.CONTENT_URI,
				    null,
				    WeatherProvider.LocationSelection,
				    new String[]{String.valueOf(lat), String.valueOf(lon)},
				    null);

		    long locationId = -1;
		    if(!cursor.moveToFirst())
		    {
			    ContentValues locationValues = new ContentValues();
			    locationValues.put(LocationEntry.COLUMN_COORD_LAT, LocationEntry.roundToLessPrecision(lat));
			    locationValues.put(LocationEntry.COLUMN_COORD_LONG, LocationEntry.roundToLessPrecision(lon));
			    Uri locationUri = getContentResolver().insert(LocationEntry.CONTENT_URI, locationValues);
			    locationId = ContentUris.parseId(locationUri);
		    }
		    else
		    {

			    locationId = cursor.getLong(cursor.getColumnIndex(LocationEntry._ID));
		    }

		    String location = WeatherEntry.getLocationString(LocationEntry.roundToLessPrecision(lat), LocationEntry.roundToLessPrecision(lon));

		    // Fetch Today
		    GregorianCalendar now = new GregorianCalendar();
		    now.set(Calendar.HOUR_OF_DAY, 0);
		    String date = String.valueOf(now.getTime().getTime() / 1000);

		    StringBuilder url = new StringBuilder(BASE_URI);
		    url.append(location);
            url.append(",").append(date);
		    url.append("?exclude=");
		    url.append("currently,");
		    url.append("minutely,");
		    url.append("daily,");
		    url.append("flags");
		    url.append("&extend=");
		    url.append("hourly");

		    fetchWeather(url.toString(), locationId);

		    // Fetch forecast
		    url = new StringBuilder(BASE_URI);
		    url.append(location);
		    url.append("?exclude=");
		    url.append("currently,");
		    url.append("minutely,");
		    url.append("daily,");
		    url.append("flags");
		    url.append("&extend=");
		    url.append("hourly");

		    fetchWeather(url.toString(), locationId);
	    }
    }

	private void fetchWeather(String url, long locationId)
	{
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        ArrayList<ContentValues> forecasts = null;

        try
        {
            // Execute the request
            HttpResponse response = httpclient.execute(httpget);
            Log.i(TAG, response.getStatusLine().toString());
            if(response.getStatusLine().getStatusCode() != 200)
            {
                Log.i(TAG, url.toString());
            }

            // Get hold of the response entity and parse the json
            HttpEntity entity = response.getEntity();
            if(entity != null)
            {
                forecasts = new ArrayList<ContentValues>();

                String jsonString = inputStreamToString(entity.getContent());
                JSONObject json = new JSONObject(jsonString).getJSONObject("hourly");
                JSONArray hours = json.getJSONArray("data");

                JSONObject hour = null;
                ContentValues values = null;

                for(int i = 0; i < hours.length(); i++)
                {
                    values = new ContentValues();

                    hour = hours.getJSONObject(i);
                    Date forecastDate = new Date(hour.getInt("time") * 1000L);

                    values.put(WeatherEntry.COLUMN_LOC_KEY, locationId);
                    values.put(WeatherEntry.COLUMN_DATETEXT, WeatherContract.getDatabaseDateString(forecastDate));
                    values.put(WeatherEntry.COLUMN_HOURTEXT, WeatherContract.getDatabaseHourString(forecastDate));
                    values.put(WeatherEntry.COLUMN_SHORT_DESC, hour.getString("summary"));
                    values.put(WeatherEntry.COLUMN_WEATHER_ICON, hour.getString("icon"));
                    values.put(WeatherEntry.COLUMN_TEMPERATURE, hour.getDouble("temperature"));
                    values.put(WeatherEntry.COLUMN_PRECIPITATION, hour.getDouble("precipProbability"));
                    values.put(WeatherEntry.COLUMN_WIND_SPEED, hour.getDouble("windSpeed"));
                    values.put(WeatherEntry.COLUMN_WIND_DEGREES, hour.getDouble("windBearing"));

                    forecasts.add(values);
                }
            }
        }
        catch(IOException ioe)
        {
            Log.e(TAG, "Failed to retrieve forecast data: " + ioe.getMessage());
        }
        catch(JSONException jse)
        {
            Log.e(TAG, "Failed to parse forecast data: " + jse.getMessage());
        }

        ContentValues[] valuesArray = new ContentValues[forecasts.size()];
        forecasts.toArray(valuesArray);
        getContentResolver().bulkInsert(WeatherEntry.CONTENT_URI, valuesArray);
    }

    private String inputStreamToString(InputStream stream) throws IOException
    {
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        StringBuilder responseStrBuilder = new StringBuilder();

        String inputStr;
        while ((inputStr = streamReader.readLine()) != null) responseStrBuilder.append(inputStr);
        return responseStrBuilder.toString();
    }
}
