package productions.moo.goldenhour.weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import productions.moo.goldenhour.weather.WeatherManager.HourlyForecast;
import productions.moo.goldenhour.weather.WeatherManager.Alert;

public class ForecastIO
{
	private static final String TAG = "ForecastIO";
	private static final String API_KEY = "013a4631b907187fffb217784ef459e6";
	private static final String BASE_URL = "https://api.forecast.io/forecast/" + API_KEY + "/";
	
	private ForecastIOListener _listener;
	
	private class ForecastParams
	{
		public Location location;
		public long unixTime;

		public ForecastParams(Location loc, long time)
		{
			location = loc;
			unixTime = time;
		}
	}
	
	static interface ForecastIOListener
	{
		public void hourlyForecasts(long requestTime, ArrayList<HourlyForecast> forecasts);
	}
	
	public ForecastIO(ForecastIOListener listener)
	{
		_listener = listener;
	}
	
	void requestHourlyForcast(Location location)
	{
		new HourlyRequest().execute(new ForecastParams(location, 0));
	}
	
	void requestHourlyForcast(Location location, long unixTime)
	{
		new HourlyRequest().execute(new ForecastParams(location, unixTime));
	}
	
	private class HourlyRequest extends AsyncTask<ForecastParams, Float, ArrayList<HourlyForecast>>
	{
		private long _requestTime;

		@Override
		protected ArrayList<HourlyForecast> doInBackground(ForecastParams... params)
		{
			ForecastParams param = params[0];
			_requestTime = param.unixTime;

			StringBuilder url = new StringBuilder(BASE_URL);
			url.append(param.location.getLatitude()).append(",");
			url.append(param.location.getLongitude());
			if(param.unixTime != 0) url.append(",").append(param.unixTime);
			url.append("?exclude=");
			url.append("currently,");
			url.append("minutely,");
			url.append("daily,");
			url.append("flags");
			url.append("&extend=");
			url.append("hourly");

			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet(url.toString());
			ArrayList<HourlyForecast> forecasts = null;

			try
			{
				// Execute the request
				HttpResponse response = httpclient.execute(httpget);
				Log.i(TAG, response.getStatusLine().toString());
				if(response.getStatusLine().getStatusCode() != 200)
				{
					Log.i(TAG, url.toString());
				}

				// Get hold of the response entity and parse the json
				HttpEntity entity = response.getEntity();
				if(entity != null)
				{
					forecasts = new ArrayList<HourlyForecast>();
					
					String jsonString = inputStreamToString(entity.getContent());
					JSONObject json = new JSONObject(jsonString).getJSONObject("hourly");
					JSONArray hours = json.getJSONArray("data");
					
					JSONObject hour = null;
					HourlyForecast forecast = null;
					
					for(int i = 0; i < hours.length(); i++)
					{
						hour = hours.getJSONObject(i);
						forecast = new HourlyForecast();

						forecast.unixTime = hour.getInt("time");
						forecast.summary = hour.getString("summary");
						forecast.icon = hour.getString("icon");
						if(hour.has("precipIntensity")) forecast.precipIntensity = hour.getDouble("precipIntensity");
						if(hour.has("precipProbability")) forecast.precipProbability = hour.getDouble("precipProbability");
						if(hour.has("temperature")) forecast.temperature = hour.getDouble("temperature");
						if(hour.has("apparentTemperature")) forecast.apparentTemperature = hour.getDouble("apparentTemperature");
						if(hour.has("dewPoint")) forecast.dewPoint = hour.getDouble("dewPoint");
						if(hour.has("windSpeed")) forecast.windSpeed = hour.getDouble("windSpeed");
						if(hour.has("windBearing")) forecast.windBearing = hour.getDouble("windBearing");
						if(hour.has("cloudCover")) forecast.cloudCover = hour.getDouble("cloudCover");
						if(hour.has("humidity")) forecast.humidity = hour.getDouble("humidity");
						if(hour.has("pressure")) forecast.pressure = hour.getDouble("pressure");
						if(hour.has("visibility")) forecast.visibility = hour.getDouble("visibility");
						if(hour.has("ozone")) forecast.ozone = hour.getDouble("ozone");
						
						if(hour.has("alerts"))
						{
							// TODO: Parse Alerts
							JSONArray alerts = hour.getJSONArray("alerts");
							
							JSONObject alertJson = null;
							Alert alert = null;

							for(int j = 0; j < alerts.length(); j++)
							{
								alertJson = alerts.getJSONObject(j);
								alert = new Alert();

								alert.title = alertJson.getString("title");
								alert.expires = alertJson.getLong("expires");
								alert.description = alertJson.getString("description");
								alert.uri = alertJson.getString("uri");
								
								forecast.alerts.add(alert);
							}
						}
						
						forecasts.add(forecast);
					}
					
					
				}
			}
			catch(IOException ioe)
			{
				Log.e(TAG, "Failed to retrieve forecast data: " + ioe.getMessage());
			}
			catch(JSONException jse)
			{
				Log.e(TAG, "Failed to parse forecast data: " + jse.getMessage());
			}

			return forecasts;
		}

		@Override
		protected void onPostExecute(ArrayList<HourlyForecast> result)
		{
			_listener.hourlyForecasts(_requestTime, result);
		}
	}
	
	private String inputStreamToString(InputStream stream) throws IOException
	{
		BufferedReader streamReader = new BufferedReader(new InputStreamReader(stream, "UTF-8")); 
	    StringBuilder responseStrBuilder = new StringBuilder();

	    String inputStr;
	    while ((inputStr = streamReader.readLine()) != null) responseStrBuilder.append(inputStr);
	    return responseStrBuilder.toString();
	}
}
