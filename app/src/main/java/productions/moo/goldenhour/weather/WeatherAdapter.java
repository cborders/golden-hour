package productions.moo.goldenhour.weather;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import productions.moo.goldenhour.R;
import productions.moo.goldenhour.calc.SolarCalculations;
import productions.moo.goldenhour.calc.SolarCalculations.SolarSeries;
import productions.moo.goldenhour.data.WeatherContract;
import productions.moo.goldenhour.data.WeatherContract.WeatherEntry;
import productions.moo.goldenhour.data.WeatherContract.LocationEntry;

/**
 * Created by cborders on 12/28/14.
 */
public class WeatherAdapter extends CursorAdapter
{
	private Context _context;
    private SimpleDateFormat _dateFormatter;

    public WeatherAdapter(Context context)
    {
        super(context, null, false);
	    _context = context;
        _dateFormatter = new SimpleDateFormat("hh:mm a");
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_small, parent, false);

        ViewHolder holder = new ViewHolder();
        holder.sunriseTime = (TextView)view.findViewById(R.id.sunrise_time);
        holder.sunriseTemp = (TextView)view.findViewById(R.id.sunrise_temp);
        holder.sunsetTime = (TextView)view.findViewById(R.id.sunset_time);
        holder.sunsetTemp = (TextView)view.findViewById(R.id.sunset_temp);
        holder.weatherIcon = (ImageView)view.findViewById(R.id.weather_icon);
        view.setTag(holder);

        updateData(view, cursor);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        updateData(view, cursor);
    }

    private void updateData(View view, Cursor cursor)
    {
        String dateString = cursor.getString(cursor.getColumnIndex(WeatherEntry.COLUMN_DATETEXT));
        Date date = WeatherContract.getDateFromDatabase(dateString);

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        double lat = cursor.getDouble(cursor.getColumnIndex(LocationEntry.COLUMN_COORD_LAT));
        double lon = cursor.getDouble(cursor.getColumnIndex(LocationEntry.COLUMN_COORD_LONG));

        String dateText = cursor.getString(cursor.getColumnIndex(WeatherEntry.COLUMN_DATETEXT));

        SolarSeries series = SolarCalculations.calculate(calendar, lat, lon);

	    Uri sunriseUri = WeatherEntry.buildWeatherLocationWithDateAndHour(lat, lon, series.Sunrise
			    .time);
	    Cursor sunriseCursor = _context.getContentResolver().query(sunriseUri, null, null, null, null);
	    float sunriseTemp = 0;
	    if(sunriseCursor.moveToFirst())
	    {
		    sunriseTemp = sunriseCursor.getFloat(sunriseCursor.getColumnIndex(WeatherEntry.COLUMN_TEMPERATURE));
	    }
	    sunriseCursor.close();

	    Uri sunsetUri = WeatherEntry.buildWeatherLocationWithDateAndHour(lat, lon, series.Sunset.time);
	    Cursor sunsetCursor = _context.getContentResolver().query(sunsetUri, null, null, null, null);
	    float sunsetTemp = 0;
	    if(sunsetCursor.moveToFirst())
	    {
		    sunsetTemp = sunsetCursor.getFloat(sunsetCursor.getColumnIndex(WeatherEntry.COLUMN_TEMPERATURE));
	    }
	    sunsetCursor.close();

	    calendar.set(Calendar.HOUR_OF_DAY, 12);
	    int icon = WeatherManager.StringToResource(cursor.getString(cursor.getColumnIndex(WeatherEntry.COLUMN_WEATHER_ICON)));
	    Uri middayUri = WeatherEntry.buildWeatherLocationWithDateAndHour(lat, lon, calendar.getTime());
	    Cursor middayCursor = _context.getContentResolver().query(middayUri, null, null, null, null);
	    if(middayCursor.moveToFirst())
	    {
		    icon = WeatherManager.StringToResource(middayCursor.getString(middayCursor
				    .getColumnIndex(WeatherEntry.COLUMN_WEATHER_ICON)));
	    }
	    middayCursor.close();

	    ViewHolder holder = (ViewHolder)view.getTag();
	    holder.weatherIcon.setImageResource(icon);
        holder.sunriseTime.setText(_dateFormatter.format(series.Sunrise.time));
        holder.sunsetTime.setText(_dateFormatter.format(series.Sunset.time));
        holder.sunriseTemp.setText(_context.getString(R.string.format_temperature, sunriseTemp));
        holder.sunsetTemp.setText(_context.getString(R.string.format_temperature, sunsetTemp));
    }

    private static class ViewHolder
    {
        TextView sunriseTime;
        TextView sunriseTemp;
        TextView sunsetTime;
        TextView sunsetTemp;
        ImageView weatherIcon;
    }
}
