package productions.moo.goldenhour.weather;

import java.util.ArrayList;

import android.location.Location;
import android.os.Handler;
import android.util.Log;

import productions.moo.goldenhour.R;
import productions.moo.goldenhour.weather.ForecastIO.ForecastIOListener;

public class WeatherManager implements ForecastIOListener
{
	private static final String TAG = "WeatherManager";
	private static final long UNIX_HOUR = 60 * 60;
	private static final int KILOMETER = 1000;
	private static final int SIGNIFIGANT_DISTANCE = KILOMETER;
	private static final int MAX_RETRIES = 5;
	private static final int RETRY_INTERVAL = 30000; // 30 seconds
	private static WeatherManager _thiz;
	
	private static WeatherTreeSet _weather;
	private ForecastIO _api;
	private ArrayList<Request> _requests;
	
	private boolean _apiIsRunning;
	private int _failedApi;
	private Handler _handler;

	private Location _location;
	
	private class Request
	{
		public Request(int requestId, long unixTime, WeatherListener listener)
		{
			this.requestId = requestId;
			this.unixTime = unixTime;
			this.listener = listener;
		}

		int requestId;
		long unixTime;
		WeatherListener listener;
	}
	
	public static class Alert
	{
		public String title;
		public long expires;
		public String description;
		public String uri;
	}
	
	public static class HourlyForecast
	{
		public long unixTime;
		public String summary;
		public String icon;
		public double precipIntensity;
		public double precipProbability;
		public double temperature;
		public double apparentTemperature;
		public double dewPoint;
		public double humidity;
		public double windSpeed;
		public double windBearing;
		public double visibility;
		public double cloudCover;
		public double pressure;
		public double ozone;
		public ArrayList<Alert> alerts = new ArrayList<Alert>();
	}
	
	public static interface WeatherListener
	{
		public void recieveWeather(int requestId, long unixTime, HourlyForecast weather);
	}

	private WeatherManager()
	{
		_weather = new WeatherTreeSet();
		_api = new ForecastIO(this);
		_requests = new ArrayList<Request>();
		_handler = new Handler();
	}
	
	public static WeatherManager getInstance()
	{
		if(_thiz == null) _thiz = new WeatherManager();
		return _thiz;
	}

	public void setLocation(Location location)
	{
		if(_location != null && !signifigantChange(location, _location)) return;

		_location = location;
		_weather.clear();
		weatherAPI(_location);
	}
	
	public void requestWeather(int requestId, long unixTime, WeatherListener listener)
	{
		HourlyForecast result = _weather.nearest(unixTime);
		
		// If the result is null or more than an hour away from the requested
		// time we need to hit the weather api
		if(result == null || Math.abs(result.unixTime - unixTime) > UNIX_HOUR)
		{
			// Add time to request queue
			_requests.add(new Request(requestId, unixTime, listener));
			
			// Hit API
			if(!_apiIsRunning)
			{
				weatherAPI(unixTime, _location);
			}
		}
		else
		{
			listener.recieveWeather(requestId, unixTime, result);
		}
	}
	
	private boolean signifigantChange(Location newLoc, Location oldLoc)
	{
		return newLoc.distanceTo(oldLoc) > SIGNIFIGANT_DISTANCE;
	}
	
	private void weatherAPI(long unixTime, Location location)
	{
		_apiIsRunning = true;
		Log.d(TAG, "Requesting weather for: " + String.valueOf(unixTime));
		_api.requestHourlyForcast(location, unixTime);
	}
	
	private void weatherAPI(Location location)
	{
		_apiIsRunning = true;
		Log.d(TAG, "Requesting weather for current time");
		_api.requestHourlyForcast(location);
	}

	/*
	 *	0 - Thunderstorms
		1 - Windy Rain
		2 - Windy Rain
		3 - Thunderstorms
		4 - T-Storms
		5 - Rain Snow
		6 - Rain Sleet
		7 - Snow/Rain Icy Mix
		8 - Freezing Drizzle
		9 - Drizzle
		10 - Freezing Rain
		11 - T-Showers / Showers / Light Rain
		12 - Heavy Rain
		13 - Snow Flurries
		14 - Light Snow
		15 - Snowflakes
		16 - Heavy Snow
		17 - Thunderstorms
		18 - Hail
		19 - Dust
		20 - Fog
		21 - Haze
		22 - Smoke
		23 - Windy
		24 - Windy
		25 - Frigid
		26 - Cloudy
		27 - Mostly Cloudy Night
		28 - Mostly Cloudy
		29 - Partly Cloudy Night
		30 - Partly Cloudy
		31 - Clear Night
		32 - Sunny
		33 - Fair / Mostly Clear Night
		34 - Fair / Mostly Sunny
		35 - Thunderstorms
		36 - Hot
		37 - Isolated Thunder
		38 - Scattered T-Storms
		39 - Scattered Rain
		40 - Heavy Rain
		41 - Scattered Snow
		42 - Heavy Snow
		43 - Windy/Snowy
		44 - Partly Cloudy Day
		45 - Scattered Showers Night
		46 - Snowy Night
		47 - Scattered T-Storms Night
	 */
	public static int StringToResource(String weather)
	{
		if(weather.equalsIgnoreCase("clear-day")) return R.drawable.weather_32;
		else if(weather.equalsIgnoreCase("clear-night")) return R.drawable.weather_31;
		else if(weather.equalsIgnoreCase("rain")) return R.drawable.weather_5;
		else if(weather.equalsIgnoreCase("snow")) return R.drawable.weather_13;
		else if(weather.equalsIgnoreCase("sleet")) return R.drawable.weather_6;
		else if(weather.equalsIgnoreCase("wind")) return R.drawable.weather_24;
		else if(weather.equalsIgnoreCase("fog")) return R.drawable.weather_20;
		else if(weather.equalsIgnoreCase("cloudy")) return R.drawable.weather_26;
		else if(weather.equalsIgnoreCase("partly-cloudy-day")) return R.drawable.weather_30;
		else if(weather.equalsIgnoreCase("partly-cloudy-night")) return R.drawable.weather_29;
		else return R.drawable.weather_na;
	}

	@Override
	public void hourlyForecasts(long requestTime, ArrayList<HourlyForecast> forecasts)
	{
		// Something, somewhere went wrong and we did get any weather
		if(forecasts == null || forecasts.size() == 0)
		{
			Log.d(TAG, "Failed to fetch weather.");

			// So we're going to increment the failed count
			_failedApi++;
			
			// And if we haven't tried too many times, we'll try again later
			if(_failedApi < MAX_RETRIES)
			{
				_handler.postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						Log.d(TAG, "Trying agin to fetch weather.");
						weatherAPI(_location);
					}
				}, RETRY_INTERVAL);
			}
			else
			{
				Log.d(TAG, "Too many retries, giving up.");
			}
			
			return;
		}

		for(HourlyForecast forecast : forecasts)
		{
			_weather.add(forecast);
		}
		
		_apiIsRunning = false;
		ArrayList<Request> reqs = new ArrayList<WeatherManager.Request>(_requests);
		_requests.clear();

		for(Request request : reqs)
		{
			requestWeather(request.requestId, request.unixTime, request.listener);
		}
		
		
	}
}
