package productions.moo.goldenhour.weather;

import java.util.Comparator;
import java.util.TreeSet;
import productions.moo.goldenhour.weather.WeatherManager.HourlyForecast;

public class WeatherTreeSet extends TreeSet<HourlyForecast>
{
	/**
	 * No idea what this means!
	 */
	private static final long serialVersionUID = -4233883718990785595L;

	private static class WeatherComparator implements Comparator<HourlyForecast>
	{
		@Override
		public int compare(HourlyForecast lhs, HourlyForecast rhs)
		{
			return (int)(rhs.unixTime - lhs.unixTime);
		}
	}
	
	public WeatherTreeSet()
	{
		super(new WeatherComparator());
	}
	
	public HourlyForecast nearest(long unixTime)
	{
		HourlyForecast request = new HourlyForecast();
		request.unixTime = unixTime;
		
		HourlyForecast floor = floor(request);
		HourlyForecast ceil = ceiling(request);
		
		if(floor == null && ceil == null) return null;

		long deltaFloor = floor == null ? Long.MAX_VALUE : floor.unixTime - unixTime;
		long deltaCeil = ceil == null ? Long.MAX_VALUE : unixTime - ceil.unixTime;
		
		return (deltaCeil < deltaFloor ? ceil : floor);
	}
}
