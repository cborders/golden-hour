package productions.moo.goldenhour;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import productions.moo.goldenhour.calc.SolarCalculations;
import productions.moo.goldenhour.calc.SolarCalculations.SolarSeries;

/**
 * Created by cborders on 12/21/14.
 */
public class DateListAdapter extends BaseAdapter
{
    private LayoutInflater _inflater;
    private SimpleDateFormat _dateFormatter;
    private Location _location;

    public DateListAdapter(Context context)
    {
        _inflater = LayoutInflater.from(context);
        _dateFormatter = new SimpleDateFormat("hh:mm a");
    }

    public void setLocation(Location location)
    {
        _location = location;
        notifyDataSetChanged();
    }

    @Override
    public int getCount()
    {
        return _location == null ? 0 : 14;
    }

    @Override
    public Object getItem(int position)
    {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DAY_OF_MONTH, position);
        return SolarCalculations.calculate(calendar, _location.getLatitude(), _location.getLongitude());
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        SmallViewHolder holder;
        SolarSeries series = (SolarSeries)getItem(position);

        if(convertView == null)
        {
            convertView = _inflater.inflate(R.layout.list_item_small, parent, false);

            holder = new SmallViewHolder();
            holder.sunriseTime = (TextView)convertView.findViewById(R.id.sunrise_time);
            holder.sunriseTemp = (TextView)convertView.findViewById(R.id.sunrise_temp);
            holder.sunsetTime = (TextView)convertView.findViewById(R.id.sunset_time);
            holder.sunsetTemp = (TextView)convertView.findViewById(R.id.sunset_temp);
            holder.weather = (ImageView)convertView.findViewById(R.id.weather_icon);

            convertView.setTag(holder);
        }
        else
        {
            holder = (SmallViewHolder)convertView.getTag();
        }

        holder.sunriseTime.setText(_dateFormatter.format(series.Sunrise.time));
        holder.sunsetTime.setText(_dateFormatter.format(series.Sunset.time));
        return convertView;
    }

    private class SmallViewHolder
    {
        TextView sunriseTime;
        TextView sunriseTemp;
        TextView sunsetTime;
        TextView sunsetTemp;
        ImageView weather;
    }
}
