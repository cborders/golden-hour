package productions.moo.goldenhour;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;

public class MainActivity extends ActionBarActivity implements DateListFragment.OnDateSelectionListener, LocationListener
{
    private static final String TAG = "GoldenHour MainActivity";
    private static final String PREF_LAT = "Latitude";
    private static final String PREF_LON = "Longitude";
    private static final float DESIRED_ACCURACY = 25.0f;

    private SharedPreferences _prefs;

    private double _lat, _lon;

    private LocationManager _locationManager;
    private DateListFragment _dateListFragment;
    private DetailFragment _detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _prefs = PreferenceManager.getDefaultSharedPreferences(this);

        _lat = _prefs.getFloat(PREF_LAT, 39.8658779f);
        _lon = _prefs.getFloat(PREF_LON, -83.0756212f);

        _locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        _dateListFragment = (DateListFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_list);
        _dateListFragment.setLocation(new LatLng(_lat, _lon));

        View detailContainer = findViewById(R.id.detail_container);
        if(detailContainer != null)
        {
            _detailFragment = DetailFragment.newInstance(_lat, _lon, new Date());
            getSupportFragmentManager().beginTransaction().replace(R.id.detail_container, _detailFragment).commit();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        _locationManager.requestLocationUpdates(1000, 1, new Criteria(), this, this.getMainLooper());
    }

    @Override
    public void onSelection(Date date, double lat, double lon)
    {
        if(_detailFragment != null)
        {
            _detailFragment.updateData(new LatLng(_lat, _lon), date);
        }
        else
        {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailFragment.DATE_KEY, date);
            intent.putExtra(DetailFragment.LAT_KEY, lat);
            intent.putExtra(DetailFragment.LON_KEY, lon);
            startActivity(intent);
        }
    }

    @Override
    public void onLocationChanged(Location location)
    {
        Log.d(TAG, "Accuracy: " + location.getAccuracy() + "m");
        if(location.getAccuracy() <= DESIRED_ACCURACY)
        {
            SharedPreferences.Editor edit = _prefs.edit();
            edit.putFloat(PREF_LAT, (float)_lat);
            edit.putFloat(PREF_LON, (float)_lon);
            edit.commit();

            LatLng latLon = new LatLng(_lat, _lon);
            _dateListFragment.setLocation(latLon);
            if(_detailFragment != null)
            {
                _detailFragment.setLocation(latLon);
            }
            _locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

    @Override
    public void onProviderEnabled(String provider)
    {

    }

    @Override
    public void onProviderDisabled(String provider)
    {

    }
}
