package productions.moo.goldenhour.calc;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import android.location.Location;

public class SolarCalculations
{
	private static final double TWILIGHT = 6.0; // Degrees
	private static final double ZENITH_RISE_SET = 90.833;  // Degrees
	private static final double ZENITH_BLUE = 90 + TWILIGHT; // Degrees
	private static final double ZENITH_GOLD = 90 - TWILIGHT; // Degrees
	
	public static class SolarPositon
	{
		public SolarPositon(Date t, double az)
		{
			time = t;
			azimuth = az;
		}

		public Date time;
		public double azimuth;
	}
	
	public static class SolarSeries
	{
		public SolarPositon BlueHourStart;
		public SolarPositon BlueHourEnd;
		
		public SolarPositon GoldenHourStart;
		public SolarPositon GoldenHourEnd;
		
		public SolarPositon Sunrise;
		public SolarPositon Sunset;
		
		public SolarPositon Current;
	}
	
	private static double calcTimeJulianCent(double jd)
	{
		double T = (jd - 2451545.0) / 36525.0;
		return T;
	}

	private static double radToDeg(double angleRad)
	{
		return(180.0 * angleRad / Math.PI);
	}

	private static double degToRad(double angleDeg)
	{
		return(Math.PI * angleDeg / 180.0);
	}

	private static double calcGeomMeanLongSun(double t)
	{
		double L0 = 280.46646 + t * (36000.76983 + t * (0.0003032));
		while(L0 > 360.0)
		{
			L0 -= 360.0;
		}
		while(L0 < 0.0)
		{
			L0 += 360.0;
		}
		return L0; // in degrees
	}

	private static double calcGeomMeanAnomalySun(double t)
	{
		double M = 357.52911 + t * (35999.05029 - 0.0001537 * t);
		return M; // in degrees
	}

	private static double calcEccentricityEarthOrbit(double t)
	{
		double e = 0.016708634 - t * (0.000042037 + 0.0000001267 * t);
		return e; // unitless
	}

	private static double calcSunEqOfCenter(double t)
	{
		double m = calcGeomMeanAnomalySun(t);
		double mrad = degToRad(m);
		double sinm = Math.sin(mrad);
		double sin2m = Math.sin(mrad + mrad);
		double sin3m = Math.sin(mrad + mrad + mrad);
		double C = sinm * (1.914602 - t * (0.004817 + 0.000014 * t)) + sin2m * (0.019993 - 0.000101 * t) + sin3m * 0.000289;
		return C; // in degrees
	}

	private static double calcSunTrueLong(double t)
	{
		double l0 = calcGeomMeanLongSun(t);
		double c = calcSunEqOfCenter(t);
		double O = l0 + c;
		return O; // in degrees
	}

//	private static double calcSunTrueAnomaly(double t)
//	{
//		double m = calcGeomMeanAnomalySun(t);
//		double c = calcSunEqOfCenter(t);
//		double v = m + c;
//		return v; // in degrees
//	}

//	private static double calcSunRadVector(double t)
//	{
//		double v = calcSunTrueAnomaly(t);
//		double e = calcEccentricityEarthOrbit(t);
//		double R = (1.000001018 * (1 - e * e)) / (1 + e * Math.cos(degToRad(v)));
//		return R; // in AUs
//	}

	private static double calcSunApparentLong(double t)
	{
		double o = calcSunTrueLong(t);
		double omega = 125.04 - 1934.136 * t;
		double lambda = o - 0.00569 - 0.00478 * Math.sin(degToRad(omega));
		return lambda; // in degrees
	}

	private static double calcMeanObliquityOfEcliptic(double t)
	{
		double seconds = 21.448 - t * (46.8150 + t * (0.00059 - t * (0.001813)));
		double e0 = 23.0 + (26.0 + (seconds / 60.0)) / 60.0;
		return e0; // in degrees
	}

	private static double calcObliquityCorrection(double t)
	{
		double e0 = calcMeanObliquityOfEcliptic(t);
		double omega = 125.04 - 1934.136 * t;
		double e = e0 + 0.00256 * Math.cos(degToRad(omega));
		return e; // in degrees
	}

	private static double calcSunDeclination(double t)
	{
		double e = calcObliquityCorrection(t);
		double lambda = calcSunApparentLong(t);

		double sint = Math.sin(degToRad(e)) * Math.sin(degToRad(lambda));
		double theta = radToDeg(Math.asin(sint));
		return theta; // in degrees
	}

	private static double calcEquationOfTime(double t)
	{
		double epsilon = calcObliquityCorrection(t);
		double l0 = calcGeomMeanLongSun(t);
		double e = calcEccentricityEarthOrbit(t);
		double m = calcGeomMeanAnomalySun(t);

		double y = Math.tan(degToRad(epsilon) / 2.0);
		y *= y;

		double sin2l0 = Math.sin(2.0 * degToRad(l0));
		double sinm = Math.sin(degToRad(m));
		double cos2l0 = Math.cos(2.0 * degToRad(l0));
		double sin4l0 = Math.sin(4.0 * degToRad(l0));
		double sin2m = Math.sin(2.0 * degToRad(m));

		double Etime = y * sin2l0 - 2.0 * e * sinm + 4.0 * e * y * sinm * cos2l0 - 0.5 * y * y * sin4l0 - 1.25 * e * e * sin2m;
		return radToDeg(Etime) * 4.0; // in minutes of time
	}

//	private static double calcHourAngleSunrise(double lat, double solarDec)
//	{
//		double latRad = degToRad(lat);
//		double sdRad = degToRad(solarDec);
//		double HAarg = (Math.cos(degToRad(90.833)) / (Math.cos(latRad) * Math.cos(sdRad)) - Math.tan(latRad) * Math.tan(sdRad));
//		double HA = Math.acos(HAarg);
//		return HA;
//	}
	
	private static double calcHourAngleAtZenithAngle(double zenith, double lat, double solarDec)
	{
		double latRad = degToRad(lat);
		double sdRad = degToRad(solarDec);
		double HAarg = (Math.cos(degToRad(zenith)) / (Math.cos(latRad) * Math.cos(sdRad)) - Math.tan(latRad) * Math.tan(sdRad));
		double HA = Math.acos(HAarg);
		return HA;
	}

	private static double getJD(GregorianCalendar calendar)
	{
		int docmonth = calendar.get(Calendar.MONTH) + 1;
		int docday = calendar.get(Calendar.DAY_OF_MONTH);
		int docyear = calendar.get(Calendar.YEAR);
		if(docmonth <= 2)
		{
			docyear -= 1;
			docmonth += 12;
		}
		double A = Math.floor(docyear / 100);
		double B = 2 - A + Math.floor(A / 4);
		double JD = Math.floor(365.25 * (docyear + 4716)) + Math.floor(30.6001 * (docmonth + 1)) + docday + B - 1524.5;
		return JD;
	}

	private static double calcAzEl(double T, double localtime, double latitude, double longitude, int zone)
	{
		double eqTime = calcEquationOfTime(T);
		double theta = calcSunDeclination(T);
		
		// TODO: Equation of Time = Math.floor(eqTime*100+0.5)/100.0;
		// TODO: Solar Declination = Math.floor(theta*100+0.5)/100.0;

		double solarTimeFix = eqTime + 4.0 * longitude - 60.0 * zone;
		//double earthRadVec = calcSunRadVector(T);
		double trueSolarTime = localtime + solarTimeFix;
		double azimuth = 0;
		while(trueSolarTime > 1440)
		{
			trueSolarTime -= 1440;
		}
		double hourAngle = trueSolarTime / 4.0 - 180.0;
		if(hourAngle < -180)
		{
			hourAngle += 360.0;
		}
		double haRad = degToRad(hourAngle);
		double csz = Math.sin(degToRad(latitude)) * Math.sin(degToRad(theta)) + Math.cos(degToRad(latitude)) * Math.cos(degToRad(theta)) * Math.cos(haRad);
		if(csz > 1.0)
		{
			csz = 1.0;
		}
		else if(csz < -1.0)
		{
			csz = -1.0;
		}
		double zenith = radToDeg(Math.acos(csz));
		double azDenom = (Math.cos(degToRad(latitude)) * Math.sin(degToRad(zenith)));
		if(Math.abs(azDenom) > 0.001)
		{
			double azRad = ((Math.sin(degToRad(latitude)) * Math.cos(degToRad(zenith))) - Math.sin(degToRad(theta))) / azDenom;
			if(Math.abs(azRad) > 1.0)
			{
				if(azRad < 0)
				{
					azRad = -1.0;
				}
				else
				{
					azRad = 1.0;
				}
			}
			azimuth = 180.0 - radToDeg(Math.acos(azRad));
			if(hourAngle > 0.0)
			{
				azimuth = -azimuth;
			}
		}
		else
		{
			if(latitude > 0.0)
			{
				azimuth = 180.0;
			}
			else
			{
				azimuth = 0.0;
			}
		}
		if(azimuth < 0.0)
		{
			azimuth += 360.0;
		}
		double exoatmElevation = 90.0 - zenith;

		// Atmospheric Refraction correction
		double refractionCorrection = 0.0;
		if(exoatmElevation <= 85.0)
		{
			double te = Math.tan(degToRad(exoatmElevation));
			refractionCorrection = -20.774 / te;
			if(exoatmElevation > 5.0)
			{
				refractionCorrection = 58.1 / te - 0.07 / (te * te * te) + 0.000086 / (te * te * te * te * te);
			}
			else if(exoatmElevation > -0.575)
			{
				refractionCorrection = 1735.0 + exoatmElevation * (-518.2 + exoatmElevation * (103.4 + exoatmElevation * (-12.79 + exoatmElevation * 0.711)));
			}
			refractionCorrection = refractionCorrection / 3600.0;
		}

		//double solarZen = zenith - refractionCorrection;

		// TODO: if solarZen > 108.0 then Azimuth and Elevation make so sense
		// document.getElementById("azbox").value = "dark"
		// document.getElementById("elbox").value = "dark"
		// else
		// document.getElementById("azbox").value = Math.floor(azimuth * 100 + 0.5) / 100.0;
		// document.getElementById("elbox").value = Math.floor((90.0 - solarZen) * 100 + 0.5) / 100.0;


		return azimuth;
	}

	private static double calcSolNoon(double jd, double longitude, double timezone, boolean dst)
	{
		double tnoon = calcTimeJulianCent(jd - longitude / 360.0);
		double eqTime = calcEquationOfTime(tnoon);
		double solNoonOffset = 720.0 - (longitude * 4) - eqTime; // in minutes
		double newt = calcTimeJulianCent(jd + solNoonOffset / 1440.0);
		eqTime = calcEquationOfTime(newt);
		double solNoonLocal = 720 - (longitude * 4) - eqTime + (timezone * 60.0);// in minutes
		if(dst) solNoonLocal += 60.0;
		while(solNoonLocal < 0.0)
		{
			solNoonLocal += 1440.0;
		}
		while(solNoonLocal >= 1440.0)
		{
			solNoonLocal -= 1440.0;
		}
		return solNoonLocal;
	}

//	private static double calcSunriseSetUTC(boolean rise, double JD, double latitude, double longitude)
//	{
//		double t = calcTimeJulianCent(JD);
//		double eqTime = calcEquationOfTime(t);
//		double solarDec = calcSunDeclination(t);
//		double hourAngle = calcHourAngleSunrise(latitude, solarDec);
//		if(!rise) hourAngle = -hourAngle;
//		double delta = longitude + radToDeg(hourAngle);
//		double timeUTC = 720 - (4.0 * delta) - eqTime; // in minutes
//		return timeUTC;
//	}
	
	private static double calcHourUTC(boolean rise, double zenith, double JD, double latitude, double longitude)
	{
		double t = calcTimeJulianCent(JD);
		double eqTime = calcEquationOfTime(t);
		double solarDec = calcSunDeclination(t);
		double hourAngle = calcHourAngleAtZenithAngle(zenith, latitude, solarDec);
		if(!rise) hourAngle = -hourAngle;
		double delta = longitude + radToDeg(hourAngle);
		double timeUTC = 720 - (4.0 * delta) - eqTime; // in minutes
		return timeUTC;
	}

//	private static SolarPositon calcSunriseSet(boolean rise, GregorianCalendar calendar, double JD, double latitude, double longitude, int timezone, boolean dst)
//	{
//		double timeUTC = calcSunriseSetUTC(rise, JD, latitude, longitude);
//		double newTimeUTC = calcSunriseSetUTC(rise, JD + timeUTC / 1440.0, latitude, longitude);
//		double timeLocal = newTimeUTC + (timezone * 60.0);
//		double riseT = calcTimeJulianCent(JD + newTimeUTC / 1440.0);
//		double riseAz = calcAzEl(riseT, timeLocal, latitude, longitude, timezone);
//		timeLocal += ((dst) ? 60.0 : 0.0);
//
//		return new SolarPositon(dateFromMinutes(calendar, timeLocal), riseAz);
//	}
	
	private static SolarPositon calcHour(boolean rise, double zenith, GregorianCalendar calendar, double JD, double latitude, double longitude, int timezone, boolean dst)
	{
		double timeUTC = calcHourUTC(rise, zenith, JD, latitude, longitude);
		double newTimeUTC = calcHourUTC(rise, zenith, JD + timeUTC / 1440.0, latitude, longitude);
		double timeLocal = newTimeUTC + (timezone * 60.0);
		double riseT = calcTimeJulianCent(JD + newTimeUTC / 1440.0);
		double riseAz = calcAzEl(riseT, timeLocal, latitude, longitude, timezone);
		timeLocal += ((dst) ? 60.0 : 0.0);

		return new SolarPositon(dateFromMinutes(calendar, timeLocal), riseAz);
	}
	
	private static Date dateFromMinutes(GregorianCalendar calendar, double minutes)
	{
		minutes = Math.round(minutes);
		int secs = 0;
		//int secs = (int)((minutes - (int)Math.floor(minutes)) * 60.0);
		int hrs = (int)(minutes / 60.0);
		int mins = (int)(minutes - hrs * 60);
		
		calendar.set(Calendar.HOUR_OF_DAY, hrs);
		calendar.set(Calendar.MINUTE, mins);
		calendar.set(Calendar.SECOND, secs);
		
		return calendar.getTime();
	}

    /**
     * Calculates the solar information for the calendar and location passed in
     * @param calendar The desired day
     * @param lat The desired latitude
     * @param lon The desired longitude
     * @return SolaSeries that contains all of the information about the various sun points
     */
	public static SolarSeries calculate(GregorianCalendar calendar, double lat, double lon)
	{
		double jday = getJD(calendar);
		double tl = (calendar.get(Calendar.HOUR_OF_DAY) * 60.0) + calendar.get(Calendar.MINUTE) + (calendar.get(Calendar.SECOND) / 60.0);
		int tz = TimeZone.getDefault().getRawOffset() / (1000 * 60 * 60);
		boolean dst = TimeZone.getDefault().inDaylightTime(calendar.getTime());
		double total = jday + tl / 1440.0 - tz / 24.0;
		double T = calcTimeJulianCent(total);
		calcAzEl(T, tl, lat, lon, tz);
		calcSolNoon(jday, lon, tz, dst);
		
		SolarSeries series = new SolarSeries();
//		series.Sunrise = calcSunriseSet(true, jday, lat, lng, tz, dst);
//		series.Sunset = calcSunriseSet(false, jday, lat, lng, tz, dst);
		
		series.Sunrise = calcHour(true, ZENITH_RISE_SET, calendar, jday, lat, lon, tz, dst);
		series.Sunset = calcHour(false, ZENITH_RISE_SET, calendar, jday, lat, lon, tz, dst);
		
		series.BlueHourStart = calcHour(true, ZENITH_BLUE, calendar, jday, lat, lon, tz, dst);
		series.BlueHourEnd = calcHour(false, ZENITH_BLUE, calendar, jday, lat, lon, tz, dst);
		
		series.GoldenHourStart = calcHour(false, ZENITH_GOLD, calendar, jday, lat, lon, tz, dst);
		series.GoldenHourEnd = calcHour(true, ZENITH_GOLD, calendar, jday, lat, lon, tz, dst);

		GregorianCalendar now = new GregorianCalendar();
		double tlNow = (now.get(Calendar.HOUR_OF_DAY) * 60.0) + now.get(Calendar.MINUTE) + (now.get(Calendar.SECOND) / 60.0);
		series.Current = new SolarPositon(now.getTime(), calcAzEl(T, tlNow, lat, lon, tz));
		
		return series;
	}
}
